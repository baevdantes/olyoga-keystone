var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Contact Model
 * ==========
 */

var Contact   = new keystone.List('Contact ', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true },
});

Contact .add({
	title: { type: String, required: true },
});

Contact .register();
