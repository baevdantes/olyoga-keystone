var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Kurse Model
 * ==========
 */

var Kurse  = new keystone.List('Kurse', {
	map: { name: 'title' },
	autokey: { path: 'slug', from: 'title', unique: true },
});

Kurse.add({
	title: { type: String, required: true },
});

Kurse.register();
