function show_my_videos(data){
	html = ['<ul id="videos">'];
	$(data.data.items).each(function(item) {
		id =  item.id;
		description = item.title;
		html.push('<iframe width="560" height="315" src="//www.youtube.com/embed/'+ id +'" frameborder="0" allowfullscreen></iframe>');
	});
	$("#videos").html(html.join(''));
}
// Replace [YOUTUBE_CHANNEL] with the channel you want
// Change max-results to the number of videos you want to display
$.ajax({
	type: "GET",
	url: "https://www.googleapis.com/youtube/v3/search?key={1085199520151-drmc0e8icpa3j1jh35ejrr7fcqmoibsc.apps.googleusercontent.com}&channelId={UC29ju8bIPH5as8OGnQzwJyA}&part=snippet,id&order=date&maxResults=20\n",
	cache: false,
	dataType:'jsonp',
	success: function(data){
		show_my_videos(data);
		//If you want to see in console...
		// console.log(data);
		// console.log(data.data.items);
		// });
	}
});
